gitbook=node_modules/.bin/gitbook

.PHONY: help
help:
	@echo ""
	@echo "make clean    - clean dependencies"
	@echo "make install  - install dependencies"
	@echo "make serve    - serve book locally: http://localhost:4000"
	@echo "make build    - update the _book/ files"
	@echo ""

.PHONY: clean
clean:
	rm -rf node_modules
	rm -rf _book

.PHONY: install
install:
	npm install --progress=false --loglevel=warn
	@echo ""
	$(gitbook) install

.PHONY: serve
serve:
	$(gitbook) serve

.PHONY: build
build:
	$(gitbook) build
