= Gitlab CI. Руководство по эксплуатации

В данном руководстве собраны основные сведения необходимые администратору при работе с Gitlab CI. За более подробной и актуальной информацией следует обращаться к официальному руководству по http://doc.gitlab.com/ce/ci/[Gitlab CI] и https://gitlab.com/gitlab-org/gitlab-ci-multi-runner[Gitlab Runner].

== Работа с проектом

----
$ make help
make clean    - clean dependencies
make install  - install dependencies
make serve    - serve book locally: http://localhost:4000
make build    - update the _book/ files
----

== Полезные ссылки

* http://help.gitbook.com[GitBook Documentation]
* http://toolchain.gitbook.com[GitBook Toolchain Documentation]
* http://asciidoctor.org/docs/asciidoc-syntax-quick-reference/[AsciiDoc Syntax Quick Reference]
